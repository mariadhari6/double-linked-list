#include<iostream>
#include<curses.h>
#include <string>
#if !defined LIBRARY_LINKED_LIST
#define LIBRARY_LINKED_LIST
using namespace std;

struct anime{
    string judul, tipe, genre[10];
    string status, airing, season;
    string studio[10], producer[10];
    int episode, I, U, E;
    float rating;
};

typedef struct elemen *memori;

struct elemen
{
    anime info;
    memori next;
    memori prev;
};

struct linked_list
{
    memori mulai;
    memori akhir;
};

string dataAnime;
linked_list Data;
char converter[100];
void Buat_data(linked_list *Data);
memori Alokasi(string Judul, string Tipe, string Genre[],
               string Status, string Airing, string Season,
               string Studio[], string Producer[], int Episode,
               float Rating, int i, int u, int e);
int Pilihan();
string InputJudul(linked_list);
string InputTipe();
string InputGenre();
void PilihGenre(int pilih);
string InputStatus();
string InputAiring();
string InputSeason();
string InputStudio();
string InputProducer();
int InputEpisode();
float InputRating();
void Tambah_awal(linked_list *l, memori data_list);
void Tambah_akhir(linked_list *l, memori data_list);
void Tampilkan_data(linked_list l);
void Cari_data(string Judul);
int Hitung_data();
bool TestJudul(linked_list l, string Judul1);
bool CekJudul(linked_list l);
memori AmbilData(linked_list l, string Judul1);
void Tambah_setalah(linked_list *l, memori data_list, string Judul1, int jmlData);
void Delete_data(string Judul1, int jmlData);
void Update_data(linked_list *l, memori data_list, string Judul, int i, int u, int e);
#endif