#include "library.h"

void Buat_data(linked_list *Data){
    Data->mulai=NULL;
    Data->akhir=NULL;
}
memori Alokasi(string Judul, string Tipe, string Genre[],
               string Status, string Airing, string Season,
               string Studio[], string Producer[], int Episode,
               float Rating, int i, int u, int e){
    
    memori a=new elemen;
    a->info.judul=Judul;
    a->info.tipe=Tipe;
    for(int b=0; b<i+1; b++){
        a->info.genre[b]=Genre[b];
    }
    a->info.status=Status;
    a->info.airing=Airing;
    a->info.season=Season;
    for(int b=0; b<u+1; b++){
        a->info.studio[b]=Studio[b];
    }
    for(int b=0; b<e+1; b++){
        a->info.producer[b]=Producer[b];
    }
    a->info.episode=Episode;
    a->info.rating=Rating;
    a->info.I=i+1;
    a->info.U=u+1;
    a->info.E=e+1;

    a->next=NULL;
    a->prev=NULL;
    return a;
}
int Pilihan(){
    int pilih;
    cout<<"1.Tanpilkan Data\n2.Tambah Data Di Awal\n3.Tambah ";
    cout<<"Data Di Akhir\n4.Delete Data\n5.Cari Data\n6.Hitung ";
    cout<<"Jumlah Data\n7.Tambah Data Setelah\n8.Update Data\n9.Keluar Program\n";
    cout<<"  Pilih : ";
    cin>>pilih;
    return pilih;
}
string InputJudul(linked_list l){
    bool cek;
    do{
        cin.get();
        cout<<"Judul\t: ";
        cin.get(converter, 100);
        dataAnime=converter ;
        cek=CekJudul(l);
    }while(cek==false);
    return dataAnime;
}
string InputTipe(){
    cout<<"Tipe\t: ";
    cin>>dataAnime;
    return dataAnime;
}
string InputGenre(){
    int pilih;
    bool eror=false;
    do{
        eror=false;
        cout<<"Genre\t: 1. Action\n";
        cout<<"\t  2. Adventure\n";
        cout<<"\t  3. Animation\n";
        cout<<"\t  4. Cars\n";
        cout<<"\t  5. Comedy\n";
        cout<<"\t  6. Crime\n";
        cout<<"\t  7. Dementia\n";
        cout<<"\t  8. Demons\n";
        cout<<"\t  9. Donghua\n";
        cout<<"\t  10. Drama\n";
        cout<<"\t  11. Ecchi\n";
        cout<<"\t  12. Family\n";
        cout<<"\t  13. Fantasy\n";
        cout<<"\t  14. Fighting\n";
        cout<<"\t  15. Game\n";
        cout<<"\t  16. Harem\n";
        cout<<"\t  17. Historical\n";
        cout<<"\t  18. Horror\n";
        cout<<"\t  19. Isekai\n";
        cout<<"\t  20. Josei\n";
        cout<<"\t  21. Kids\n";
        cout<<"\t  22. Life\n";
        cout<<"\t  23. Magic\n";
        cout<<"\t  24. Martial Arts\n";
        cout<<"\t  25. Mature\n";
        cout<<"\t  26. Mecha\n";
        cout<<"\t  27. Military\n";
        cout<<"\t  28. Music\n";
        cout<<"\t  29. Mystery\n";
        cout<<"\t  30. Parody\n";
        cout<<"\t  31. Police\n";
        cout<<"\t  32. Psychological\n";
        cout<<"\t  33. Romance\n";
        cout<<"\t  34. Samurai\n";
        cout<<"\t  35. School\n";
        cout<<"\t  36. Sci-Fi\n";
        cout<<"\t  37. Seinen\n";
        cout<<"\t  38. Shoujo\n";
        cout<<"\t  39. Shoujo Ai\n";
        cout<<"\t  40. Shounen\n";
        cout<<"\t  41. Shounen Ai\n";
        cout<<"\t  42. Slice of Life\n";
        cout<<"\t  43. Space\n";
        cout<<"\t  44. Special\n";
        cout<<"\t  45. Sports\n";
        cout<<"\t  46. Super Power\n";
        cout<<"\t  47. Supernatural\n";
        cout<<"\t  48. Suspense\n";
        cout<<"\t  49. Thriller\n";
        cout<<"\t  50. Vampire\n";
        cout<<"Pilih : ";
        cin>>dataAnime;
        try
        {
            pilih=stoi(dataAnime);
        }
        catch(const std::exception& e)
        {
            eror=true;
        }
    }while(eror==true);
    
    PilihGenre(pilih);
    return dataAnime;
} 
void PilihGenre(int pilih){
    if(pilih==1){
        dataAnime="Action";
    }
    else if(pilih==2){
        dataAnime="Adventure";
    }
    else if(pilih==3){
        dataAnime="Animation";
    }
    else if(pilih==4){
        dataAnime="Cars";
    }
    else if(pilih==5){
        dataAnime="Comedy";
    }
    else if(pilih==6){
        dataAnime="Crime";
    }
    else if(pilih==7){
        dataAnime="Dementia";
    }
    else if(pilih==8){
        dataAnime="Demons";
    }
    else if(pilih==9){
        dataAnime="Donghua";
    }
    else if(pilih==10){
        dataAnime="Drama";
    }
    else if(pilih==11){
        dataAnime="Ecchi";
    }
    else if(pilih==12){
        dataAnime="Family";
    }
    else if(pilih==13){
        dataAnime="Fantasy";
    }
    else if(pilih==14){
        dataAnime="Fighting";
    }
    else if(pilih==15){
        dataAnime="Game";
    }
    else if(pilih==16){
        dataAnime="Horror";
    }
    else if(pilih==17){
        dataAnime="Historical";
    }
    else if(pilih==18){
        dataAnime="Horror";
    }
    else if(pilih==19){
        dataAnime="Isekai";
    }
    else if(pilih==20){
        dataAnime="Josei";
    }
    else if(pilih==21){
        dataAnime="Kids";
    }
    else if(pilih==22){
        dataAnime="Life";
    }
    else if(pilih==23){
        dataAnime="Magic";
    }
    else if(pilih==24){
        dataAnime="Martial Arts";
    }
    else if(pilih==25){
        dataAnime="Mature";
    }
    else if(pilih==26){
        dataAnime="Mecha";
    }
    else if(pilih==27){
        dataAnime="Military";
    }
    else if(pilih==28){
        dataAnime="Music";
    }
    else if(pilih==29){
        dataAnime="Mystery";
    }
    else if(pilih==30){
        dataAnime="Parody";
    }
    else if(pilih==31){
        dataAnime="Police";
    }
    else if(pilih==32){
        dataAnime="Psychological";
    }
    else if(pilih==33){
        dataAnime="Romance";
    }
    else if(pilih==34){
        dataAnime="Samurai";
    }
    else if(pilih==35){
        dataAnime="School";
    }
    else if(pilih==36){
        dataAnime="Sci-Fi";
    }
    else if(pilih==37){
        dataAnime="Seinen";
    }
    else if(pilih==38){
        dataAnime="Shoujo";
    }
    else if(pilih==39){
        dataAnime="Shoujo Ai";
    }
    else if(pilih==40){
        dataAnime="Shounen";
    }
    else if(pilih==41){
        dataAnime="Shounen Ai";
    }
    else if(pilih==42){
        dataAnime="Slice of Life";
    }
    else if(pilih==43){
        dataAnime="Space";
    }
    else if(pilih==44){
        dataAnime="Special";
    }
    else if(pilih==45){
        dataAnime="Sports";
    }
    else if(pilih==46){
        dataAnime="Super Power";
    }
    else if(pilih==47){
        dataAnime="Supernatural";
    }
    else if(pilih==48){
        dataAnime="Suspense";
    }
    else if(pilih==49){
        dataAnime="Thriller";
    }
    else{
        dataAnime="Vampire";
    }
}
string InputStatus(){
    int status;
    do{
        system("clear");
        cout<<"Status\t: 1. Finished\n";
        cout<<"\t  2. Ongoing\n";
        cout<<"Pilih : ";
        cin>>status;
    }while(status>2 || status<1);
    if(status==1){
        dataAnime="Finished";
    }
    else{
        dataAnime="Ongoing";
    }
    return dataAnime;
}
string InputAiring(){
    bool eror=false;
    do{
        eror=false;
        cout<<"Tahun Rilis : ";
        cin>>dataAnime;
        try
        {
            int c=stoi(dataAnime);
        }
        catch(const std::exception& e)
        {
            eror=true;
        }
    }while(eror==true);
    return dataAnime;
}
string InputSeason(){
    int pilih;
    do{
        system("clear");
        cout<<"Season\t: 1. Winter\n";
        cout<<"\t  2. Spring\n";
        cout<<"\t  3. Summer\n";
        cout<<"\t  4. Fall\n";
        cout<<"Pilih : ";
        cin>>pilih;
    }while(pilih<1 || pilih>4);
    if(pilih==1){
        dataAnime="Winter";
    }
    else if(pilih==2){
        dataAnime="Spring";
    }
    else if(pilih==3){
        dataAnime="Summer";
    }
    else{
        dataAnime="Fall";
    }
    return dataAnime;
}
string InputStudio(){
    cin.get();
    cout<<"Studio : ";
    cin.getline(converter, 100);
    dataAnime=converter;
    return dataAnime;
}
string InputProducer(){
    cin.get();
    cout<<"Producer : ";
    cin.getline(converter, 100);
    dataAnime=converter;
    return dataAnime;
}
int InputEpisode(){
    int eps;
    bool eror=false;
    do{
        eror=false;
        cout<<"Jumlah episode :";
        cin>>dataAnime;
        try
        {
            eps=stoi(dataAnime);
        }
        catch(const std::exception& e)
        {
            eror=true;
        }
    }while(eror==true);
    return eps;
}
float InputRating(){
    float rate;
    bool eror=false;
    do{
        eror=false;
        cout<<"Rating :";
        cin>>dataAnime;
        try
        {
            rate=stof(dataAnime);
        }
        catch(const std::exception& e)
        {
            eror=true;
        }
    }while(eror==true || (rate>10 || rate<0));
    return rate;
}
void Tambah_awal(linked_list *l, memori data_list){
    if(l->mulai==NULL){
        l->mulai=data_list;
        l->akhir=data_list;
    }
    else{
        data_list->next=l->mulai;
        l->mulai->prev=data_list;
        l->mulai=data_list;
    }
}
void Tambah_akhir(linked_list *l, memori data_list){
    data_list->prev=l->akhir;
    l->akhir->next=data_list;
    l->akhir=data_list;
}
void Tampilkan_data(linked_list l){
    if(l.mulai==NULL){
        cout<<"Data Kosong !!!\n";
        return;
    }
    memori a=l.mulai;
    int x=1;
    while(a!=NULL){
        cout<<"=====Data Anime Ke-"<<x<<"=====\n";
        cout<<"Judul\t= "<<a->info.judul<<endl;
        cout<<"Tipe\t= "<<a->info.tipe<<endl;
        cout<<"Genre\t: 1. "<<a->info.genre[0]<<endl;
        for(int z=1; z<a->info.I; z++){
            cout<<"\t  "<<z+1<<". "<<a->info.genre[z]<<endl;
        }
        cout<<"Status\t= "<<a->info.status<<endl;
        cout<<"Airing\t= "<<a->info.airing<<endl;
        cout<<"Season\t= "<<a->info.season<<endl;
        cout<<"Studio\t: 1. "<<a->info.studio[0]<<endl;
        for(int z=1; z<a->info.U; z++){
            cout<<"\t  "<<z+1<<". "<<a->info.studio[z]<<endl;
        }
        cout<<"Producer: 1. "<<a->info.producer[0]<<endl;
        for(int z=1; z<a->info.E; z++){
            cout<<"\t  "<<z+1<<". "<<a->info.producer[z]<<endl;
        }
        cout<<"Episodes= "<<a->info.episode<<endl;
        cout<<"Rating\t= "<<a->info.rating<<endl;
        x++;
        a=a->next;
    }
}
void Cari_data(string Judul){
    system("clear");
    if(Data.mulai==NULL){
        cout<<"Data Kosong !!!\n";
        return;
    }
    memori a=Data.mulai;
    while(a!=NULL){
        if(a->info.judul.compare(Judul)==0){
            cout<<"=======Data Anime=======\n";
            cout<<"Judul\t= "<<a->info.judul<<endl;
            cout<<"Tipe\t= "<<a->info.tipe<<endl;
            cout<<"Genre\t: 1. "<<a->info.genre[0]<<endl;
            for(int z=1; z<a->info.I; z++){
                cout<<"\t  "<<z+1<<". "<<a->info.genre[z]<<endl;
            }
            cout<<"Status\t= "<<a->info.status<<endl;
            cout<<"Airing\t= "<<a->info.airing<<endl;
            cout<<"Season\t= "<<a->info.season<<endl;
            cout<<"Studio\t: 1. "<<a->info.studio[0]<<endl;
            for(int z=1; z<a->info.U; z++){
                cout<<"\t  "<<z+1<<". "<<a->info.studio[z]<<endl;
            }
            cout<<"Producer: 1. "<<a->info.producer[0]<<endl;
            for(int z=1; z<a->info.E; z++){
                cout<<"\t  "<<z+1<<". "<<a->info.producer[z]<<endl;
            }
            cout<<"Episodes= "<<a->info.episode<<endl;
            cout<<"Rating\t= "<<a->info.rating<<endl;
            return;
        }
        a=a->next;
    }
    cin.get();
    cout<<"Data Tidak Ditemukan !!!\n";
}
int Hitung_data(){
    int jml=0;
    memori a=Data.mulai;
    while(a!=NULL){
        a=a->next;
        jml++;
    }
    return jml;
}
bool TestJudul(linked_list l, string Judul1){
    memori a=l.mulai;
    bool ada=false;
    int b;
    while(a!=NULL){
        b=Judul1.compare(a->info.judul);
        if(b==0){
            break;
        }
        a=a->next;
    }
    if(b==0){
        ada=true;
    }
    return ada;
}
bool CekJudul(linked_list l){
    memori a=l.mulai;
    int b;
    while(a!=NULL){
        b=dataAnime.compare(a->info.judul);
        if(b==0){
            break;
        }
        a=a->next;
    }
    if(b==0){
        return false;
    }
    else{
        return true;
    }
}
memori AmbilData(linked_list l, string Judul1){
    memori a=l.mulai;
    while(a!=NULL){
        if(Judul1.compare(a->info.judul)==0){
            break;
        }
        a=a->next;
    }
    return a;
}
void Tambah_setalah(linked_list *l, memori data_list, string Judul1, int jmlData){
    if(l->mulai!=NULL){
        memori a=l->mulai;
        int i=0;
        while(a!=NULL){
            i++;
            if(i==jmlData-1){
                Tambah_akhir(l, data_list);
                break;
            }
            if(a->info.judul.compare(Judul1)==0){
                data_list->next=a->next;
                data_list->prev=a;
                a->next=data_list;
                data_list->next->prev=data_list;
                return;
            }
            a=a->next;
        }
    }
}
void Delete_data(string Judul1, int jmlData){
    if(Data.mulai==NULL){
        cout<<"Data Kosong !!!\n";
        return;
    }
    memori wadah;
    if(Data.mulai->info.judul.compare(Judul1)==0){
        wadah=Data.mulai;
        Data.mulai=Data.mulai->next;
        return;
    }
    int i=0;
    memori a=Data.mulai;
    while(a!=NULL){
        i++;
        if(i==jmlData-1){
            break;
        }
        if(a->next->info.judul.compare(Judul1)==0){
            wadah=a->next;
            a->next=wadah->next;
            wadah->next->prev=wadah->prev;
            return;
        }
        a=a->next;
    }
}
void Update_data(linked_list *l, memori data_list, string Judul, int i, int u, int e){
    memori a=l->mulai;
    while(a!=NULL){
        if(a->info.judul.compare(Judul)==0){
            a->info.tipe=data_list->info.tipe;
            for(int b=0; b<i+1; b++){
                a->info.genre[b]=data_list->info.genre[b];
            }      
            a->info.status=data_list->info.status;
            a->info.airing=data_list->info.airing;
            a->info.season=data_list->info.season;
            for(int b=0; b<u+1; b++){
                a->info.studio[b]=data_list->info.studio[b];
            }
            for(int b=0; b<e+1; b++){
                a->info.producer[b]=a->info.producer[b];
            }
            a->info.episode=data_list->info.episode;
            a->info.rating=data_list->info.rating;
            a->info.I=i+1;
            a->info.U=u+1;
            a->info.E=e+1;
            return;
        }
        a=a->next;
    }
}